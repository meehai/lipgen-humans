import yaml
import matplotlib.pyplot as plt
import torch.optim as optim
import numpy as np
import torch as tr
from argparse import ArgumentParser
from pathlib import Path
from neural_wrappers.utilities import getGenerators, changeDirectory
from neural_wrappers.pytorch import device
from neural_wrappers.callbacks import SaveModels, SaveHistory, PlotMetrics, Callback
from neural_wrappers.schedulers import ReduceLRAndBacktrackOnPlateau

from readers import getReader
from models import getModel
from plot_dataset import plot_dataset
from inference_video import inference_video
from test_dataset import test_dataset
from plot_utils import RandomPlotEpoch

np.random.seed(42)
tr.manual_seed(42)

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")

	# Model config stuff
	parser.add_argument("--modelCfg")
	parser.add_argument("--weightsFile")
	# Data config stuff
	parser.add_argument("--dataCfg")
	parser.add_argument("--validationDataCfg")
	parser.add_argument("--dir")

	# Video inference parameters
	parser.add_argument("--videoPath", help="path to source video")
	parser.add_argument("--wavPath", help="path to target audio")
	parser.add_argument("--resultVideoPath", help="path to result video after applying target audio")

	args = parser.parse_args()
	assert args.type in ("plot_dataset", "train", "inference_video", "test_dataset", "test", "retrain")

	if not args.modelCfg is None:
		args.modelCfg = yaml.load(open(args.modelCfg, "r"), Loader=yaml.SafeLoader)

	if not args.dataCfg is None:
		args.dataCfg = yaml.load(open(args.dataCfg, "r"), Loader=yaml.SafeLoader)
		args.dataCfg["context"] = args.modelCfg["context"] if not args.modelCfg is None else 2

	if not args.validationDataCfg is None:
		args.validationDataCfg = yaml.load(open(args.validationDataCfg, "r"), Loader=yaml.SafeLoader)
		args.validationDataCfg["context"] = args.modelCfg["context"] if not args.modelCfg is None else 2

	if args.type == "train":
		assert not args.dataCfg is None

	if args.type == "inference_video":
		assert not args.wavPath is None
		assert not args.videoPath is None
		assert not args.resultVideoPath is None
		assert not args.weightsFile is None

	if args.type in ("retrain", "test"):
		assert not args.weightsFile is None

	if not args.type in ("inference_video", "test"):
		assert not args.dir is None

	return args

def main():
	args = getArgs()

	if args.type == "plot_dataset":
		args.dataCfg["context"] = 2
		reader = getReader(args.dataCfg)
		changeDirectory(args.dir, expectExist=False)
		plot_dataset(reader.iterateOneEpoch())
		exit()

	model = getModel(args.modelCfg).to(device)

	if args.type == "inference_video":
		model.loadWeights(args.weightsFile)
		model.eval()
		print(model.summary())
		inference_video(model, args.videoPath, args.wavPath, args.resultVideoPath)
		exit()

	reader = getReader(args.dataCfg)

	generator = reader.iterate()
	valGenerator = None
	if not args.validationDataCfg is None:
		valReader = getReader(args.validationDataCfg)
		valGenerator = valReader.iterate()

	callbacks = [SaveModels("best", "Loss"), SaveModels("last", "Loss"), \
		RandomPlotEpoch(len(generator)), SaveHistory("history.txt")]
	model.addCallbacks(callbacks)

	if args.type in ("train", "retrain"):
		optimizer = {
			"Adam" : optim.Adam,
			"AdamW" : optim.AdamW
		}[args.dataCfg["optimizer"]]
		model.setOptimizer(optimizer, lr=args.dataCfg["learningRate"])
		if "patience" in args.dataCfg:
			model.setOptimizerScheduler(ReduceLRAndBacktrackOnPlateau(model, "Loss", \
				args.dataCfg["patience"], args.dataCfg["factor"]))

	if args.type == "train":
		print(model.summary())
		changeDirectory(args.dir, expectExist=False)
		model.trainGenerator(generator, args.dataCfg["numEpochs"], valGenerator)
	elif args.type == "retrain":
		model.loadModel(args.weightsFile)
		print(model.summary())
		changeDirectory(args.dir, expectExist=None)
		model.trainGenerator(generator, args.dataCfg["numEpochs"], valGenerator)
	elif args.type == "test":
		model.loadWeights(args.weightsFile)
		model.eval()
		print(model.summary())
		res = model.testGenerator(generator, printMessage=True)
		print(res)
	elif args.type == "test_dataset":
		model.loadWeights(args.weightsFile)
		model.eval()
		print(model.summary())
		test_dataset(model, generator)

if __name__ == "__main__":
	main()