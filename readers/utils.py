import numpy as np
import h5py
import os
from argparse import ArgumentParser
from pathlib import Path
from tqdm import trange
from media_processing_lib.image import imgResize, imgResize_batch
from media_processing_lib.video import tryReadVideo, MPLVideo
from media_processing_lib.audio import tryReadAudio, MPLAudio
from media_processing_lib.audio.melspectrogram import melspectrogram
from vke.face import vidGetFaceBbox
from scipy import signal
from typing import Tuple

def getMelChunks(mel:np.ndarray, mel_step_size:int, fps:float) -> np.ndarray:
	assert np.isnan(mel.reshape(-1)).sum() == 0
	mel_chunks = []
	mel_idx_multiplier = 80./fps
	i = 0
	while 1:
		start_idx = int(i * mel_idx_multiplier)
		if start_idx + mel_step_size > len(mel[0]):
			mel_chunks.append(mel[:, len(mel[0]) - mel_step_size:])
			break
		mel_chunks.append(mel[:, start_idx : start_idx + mel_step_size])
		i += 1
	mel_chunks = np.array(mel_chunks, dtype=np.float32)
	return mel_chunks

def crop(image, bbox):
	x1, y1, x2, y2 = bbox
	res = image[y1 : y2, x1 : x2]
	if res.shape[0] == 0 or res.shape[1] == 0:
		from vke.face import imgGetFaceBbox
		_bbox = imgGetFaceBbox(image)
		print(_bbox)
		breakpoint()
	return res

def preprocessVideo(video:MPLAudio, audio:MPLAudio, faceBbox:np.ndarray, resolution:Tuple[int, int], \
	audioWindowSize:float, context:int, seed:int=42):
	np.random.seed(seed)
	windowSize_frame = int(np.ceil(audioWindowSize * video.fps))
	assert context < windowSize_frame and context >= 0, "Context: %d" % context
	halfWindow = windowSize_frame // 2
	height, width = resolution

	# source frame can be any frame between first and -windowSize//2
	sourceFrameIx = np.random.randint(context, len(video) - windowSize_frame - 1)
	# target frame is somewhere after the right side of half window (so no intersection is done)
	targetFrameIx = np.random.randint(sourceFrameIx + halfWindow, len(video) - halfWindow - 2)
	mel = melspectrogram(audio)
	melChunks = getMelChunks(mel, mel_step_size=16, fps=video.fps)
	# context == 2 => we use 5 frames (2nd one is targetFrameIx)

	# Audio stuff
	targetMel = melChunks[targetFrameIx]
	# Images stuff
	# Get & Crop face & resize images
	sourceFrame = crop(video[sourceFrameIx], faceBbox[sourceFrameIx])
	sourceFrame = imgResize(sourceFrame, height=height, width=width, resizeLib="lycon", interpolation="bilinear")
	targetFrame = crop(video[targetFrameIx], faceBbox[targetFrameIx])
	targetFrame = imgResize(targetFrame, height=height, width=width, resizeLib="lycon", interpolation="bilinear")

	# Context Audio/Images stuff
	targetMelContext, sourceContext, targetContext = None, None, None
	if context > 0:
		targetMelContextLeft = melChunks[targetFrameIx - context : targetFrameIx]
		targetMelContextRight = melChunks[targetFrameIx + 1 : targetFrameIx + context + 1]
		targetMelContext = np.concatenate([targetMelContextLeft, targetMelContextRight])

		sourceContext = [crop(video[sourceFrameIx + i], faceBbox[sourceFrameIx + i]) for i in range(1, context + 1)] +\
			[crop(video[sourceFrameIx - i], faceBbox[sourceFrameIx - i]) for i in range(1, context + 1)]
		sourceContext = imgResize_batch(sourceContext, height=height, width=width, \
			resizeLib="lycon", interpolation="bilinear")

		targetContext = [crop(video[targetFrameIx + i], faceBbox[targetFrameIx + i]) for i in range(1, context + 1)] +\
			[crop(video[targetFrameIx - i], faceBbox[targetFrameIx - i]) for i in range(1, context + 1)]
		targetContext = imgResize_batch(targetContext, height=height, width=width, \
			resizeLib="lycon", interpolation="bilinear")

	return {
		"indexes" : {
			"sourceFrameIx" : sourceFrameIx,
			"targetFrameIx" : targetFrameIx,
		},
		"frames" : {
			"sourceFrame" : sourceFrame,
			"sourceContext" : sourceContext,
			"targetFrame" : targetFrame,
			"targetContext" : targetContext,
		},
		"audio" : {
			"targetMel" : targetMel,
			"targetMelContext" : targetMelContext,
		}
	}

	return res

# Used by MergeDatasetReader
def mergeFn(items):
	n = len(items)
	Keys = ["sourceFrame", "targetFrame", "sourceContext", "targetContext", \
		"targetMel", "targetMelContext", "sourceFrameIx", "targetFrameIx"]
	catItems = {k : [] for k in Keys}

	for i in range(n):
		item = items[i]
		if item is None:
			continue
		for k in Keys:
			if k in item.keys() and not item[k] is None:
				catItems[k].append(item[k])

	resItems = {}
	for k in catItems:
		# None troubles
		if len(catItems[k]) == 0:
			resItems[k] = None
			continue
		# BatchNorm troubles
		if len(catItems[k]) == 1:
			catItems[k].append(catItems[k][0])
		resItems[k] = np.stack(catItems[k], axis=0)
	return resItems, resItems

def normalizeVideo(item):
	if item is None:
		return None

	res = {}
	for key in ["sourceFrame", "targetFrame"]:
		res[key] = np.float32(item["frames"][key].transpose(2, 0, 1).copy()) / 255
	for key in ["sourceContext", "targetContext"]:
		if not item["frames"][key] is None:
			res[key] = np.float32(item["frames"][key].transpose(0, 3, 1, 2).copy()) / 255
		else:
			res[key] = None
	# targetMel, targetMelLeftContext, targetMelRightContext
	for key in item["audio"]:
		res[key] = item["audio"][key]
	# targetFrameIx, sourceFrameIx
	for key in item["indexes"]:
		res[key] = item["indexes"][key]
	return res