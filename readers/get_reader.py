import os
from neural_wrappers.readers import StaticBatchedDatasetReader, CachedDatasetReader, \
    MergeBatchedDatasetReader, CombinedDatasetReader, RandomIndexDatasetReader
from simple_caching import NpyFS
from .video_dir_reader import VideoDirReader
from .video_finetune_reader import VideoFineTuneReader
from .video_dir_plus_keypoints_reader import VideoDirPlusKeypointsReader, mergeKpFn
from .utils import mergeFn

def getVideoDirReader(dataCfg):
    reader = VideoDirReader(dataCfg["datasetPath"], dataCfg["context"])
    cacheDir = "%s/.cache/%s" % (dataCfg["datasetPath"], reader.__cache__())
    reader = CachedDatasetReader(reader, cache=NpyFS(cacheDir), buildCache=True)
    return reader

def getVideoFineTuneReader(dataCfg):
    vidPath = os.path.realpath(os.path.abspath(dataCfg["videoPath"]))
    vidName = vidPath.split("/")[-1]
    vidDir = "/".join(vidPath.split("/")[0 : -1])
    cacheDir = "%s/.cache/%s" % (vidDir, vidName)
    reader = VideoFineTuneReader(vidPath, context=dataCfg["context"], N=dataCfg["numFrames"])
    reader = CachedDatasetReader(reader, cache=NpyFS(cacheDir), buildCache=True)
    return reader

def getVideoDirPlusKeypointsReader(dataCfg):
    baseReader = getVideoDirReader(dataCfg)
    reader = VideoDirPlusKeypointsReader(baseReader)
    cacheDir = "%s/.cache/%s" % (dataCfg["datasetPath"], reader.__cache__())
    reader = CachedDatasetReader(reader, cache=NpyFS(cacheDir), buildCache=True)
    reader = MergeBatchedDatasetReader(reader, mergeFn=mergeKpFn)
    reader = StaticBatchedDatasetReader(reader, dataCfg["batchSize"])
    return reader

def getReader(dataCfg):
    getter = {
        "VideoDirReader" : getVideoDirReader,
        "VideoFineTuneReader" : getVideoFineTuneReader,
        "VideoDirPlusKeypointsReader" : getVideoDirPlusKeypointsReader
    }

    if dataCfg["datasetType"] in ("VideoDirReader", "VideoFineTuneReader"):
        reader = getter[dataCfg["datasetType"]](dataCfg)
        reader = MergeBatchedDatasetReader(reader, mergeFn=mergeFn)
        reader = StaticBatchedDatasetReader(reader, dataCfg["batchSize"])
    elif dataCfg["datasetType"] == "CombinedReader":
        readers = []
        for readerCfg in dataCfg["datasets"]:
            readerCfg["context"] = dataCfg["context"]
            reader = getter[readerCfg["datasetType"]](readerCfg)
            readers.append(reader)
        reader = CombinedDatasetReader(readers)
        reader = MergeBatchedDatasetReader(reader, mergeFn=mergeFn)
        reader = StaticBatchedDatasetReader(reader, dataCfg["batchSize"])
        reader = RandomIndexDatasetReader(reader)
    elif dataCfg["datasetType"] == "VideoDirPlusKeypointsReader":
        reader = getVideoDirPlusKeypointsReader(dataCfg)

    print(reader)
    return reader
