# v1
import numpy as np
import torch as tr
from overrides import overrides
from .audio_encoder import AudioEncoder
from .face_encoder import FaceEncoder
from .face_decoder import FaceDecoder
from ...base_model import BaseModel
from readers.video_dir_plus_keypoints_reader import coord_to_mask
from neural_wrappers.pytorch import device, trGetData, npGetData
from neural_wrappers.utilities import npGetInfo

def reconstructionLoss(y, t):
	return (y - t).abs().mean()

class Model(BaseModel):
	def __init__(self, config):
		super().__init__(config)
		self.audioEncoder = AudioEncoder()
		self.faceEncoder = FaceEncoder(dIn=7)
		self.faceDecoder = FaceDecoder()

	@overrides
	def networkAlgorithm(self, trInputs, trLabels, isTraining, isOptimizing):
		B, D, H, W = trInputs["targetFrame"].shape
		mask = tr.ones(H, W).to(device)
		mask[H // 2:] = 0
		mel = trInputs["targetMel"].unsqueeze(dim=1)

		def f(a, t, s, sm):
			# audioSequences :: (B, 1, 80, 16)
			audioSequences = a
			# faceSequences :: (B, 6, 96, 96)
			faceSequences = tr.cat([t * mask, s, sm], dim=1)
			faceEmbeddingList = self.faceEncoder(faceSequences)
			audioEmbedding = self.audioEncoder(audioSequences)
			faceEmbedding = faceEmbeddingList[-1]
			g = self.faceDecoder(faceEmbeddingList, audioEmbedding)
			return g

		sourceKps = (trInputs["sourceKeypoints"]*tr.FloatTensor([H, W]).to("cuda"))
		keypointsMask = trGetData(coord_to_mask(npGetData(sourceKps), H, W)).unsqueeze(dim=1)
		g = f(mel, trInputs["targetFrame"], trInputs["sourceFrame"], keypointsMask)
		gt = trInputs["targetFrame"]
		lossRecon = reconstructionLoss(g, gt)

		gAll = [g]
		for i in range(self.hyperParameters["context"] * 2):
			sourceContextKps = (trInputs["sourceContextKeypoints"][:, i]*tr.FloatTensor([H, W]).to("cuda"))
			ctxKeypointsMask = trGetData(coord_to_mask(npGetData(sourceContextKps), H, W)).unsqueeze(dim=1)
			gCtx = f(trInputs["targetMelContext"][:, i : i + 1], \
				trInputs["targetContext"][:, i], trInputs["sourceContext"][:, i], ctxKeypointsMask)
			gtCtx = trInputs["targetContext"][:, i]
			lossReconCtx = reconstructionLoss(gCtx, gtCtx)
			lossRecon += lossReconCtx
			gAll.append(gCtx)
		lossRecon = lossRecon / len(gAll)
		lossSyncMiddle = self.expertSyncLoss(mel, g)

		if self.hyperParameters["context"] > 0 and self.hyperParameters["context"] * 2 + 1 == self.baseSyncNet.T:
			# Reorder from [middle, ctx] to [ctxHalfLeft, middle, ctxHalfRight]
			ctxHalfLeft = gAll[1 : self.hyperParameters["context"] + 1]
			ctxHalfRight = gAll[-self.hyperParameters["context"] : ]
			gAll = [*ctxHalfLeft, gAll[0], *ctxHalfRight]
			# gAll :: (B, 3, T, H ,W)
			gAll = tr.stack(gAll).permute(1, 2, 0, 3, 4)
			lossSync = self.expertSyncLoss(mel, gAll)
		else:
			lossSync = lossSyncMiddle

		wSync = self.hyperParameters["lossWeights"]["sync"]
		wRecon = self.hyperParameters["lossWeights"]["reconstruction"]
		loss = (wSync * lossSync + wRecon * lossRecon) / (wSync + wRecon)
		self.updateOptimizer(loss, isTraining, isOptimizing)

		trResults = {
			"targetMasked" : trInputs["targetFrame"] * mask,
			"targetGenerated" : g.detach(),
			"GLoss" : lossRecon.detach(),
			"expertSyncLoss" : lossSyncMiddle.detach()
		}

		return trResults, loss
