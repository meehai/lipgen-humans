import torch as tr
import torch.nn as nn
import numpy as np
from overrides import overrides
from neural_wrappers.pytorch import device
from neural_wrappers.callbacks import SaveModels, PlotMetrics
from .repo_code import Wav2Lip as RepoModel
from ...base_model import BaseModel, doLoad

def recon_loss(y, t):
	return (y - t).abs().mean()

class Model(BaseModel):
	def __init__(self, config):
		super().__init__(config)
		assert self.hyperParameters["context"] == 2
		self.repoModel = RepoModel()

	@overrides
	def networkAlgorithm(self, trInputs, trLabels, isTraining, isOptimizing):
		assert False, "This is just for loading pretrained and inference purposes"
		# read img names at random
		# img_name = random.choice(img_names)
		# wrong_img_name = random.choice(img_names)
		# get the window names (source & target + context)
		# window_fnames = self.get_window(img_name)
		# wrong_window_fnames = self.get_window(wrong_img_name)
		# get the frames (sourceFrame + sourceContext + targetFrame + targetContext)
		# window = self.read_window(window_fnames)
		# wrong_window = self.read_window(wrong_window_fnames)

		# SO: window == targetFrame, wrong_window = sourceFrame
		# y = window.copy() # label == targetFrame
		# window = self.prepare_window(window) ## prepare_window == transpose to channels first & /255
		# window[:, :, window.shape[2]//2:] = 0. ## targetMasked
		# wrong_window = self.prepare_window(wrong_window) ## sourceFrame
		# mel = self.crop_audio_window(orig_mel.copy(), img_name) ## mel just for targetFrame -> used for 
		#  syncloss; mel :: B, 16, 80 (?)
		# indiv_mels = self.get_segmented_mels(orig_mel.copy(), img_name) ## targetMel -> targetFrame + context -> 
		#  used for generation: g = model(indiv_mels, x) and sync_loss = get_sync_loss(mel, g). 
		#  indiv_mes :: B, T, 80, 16 with T = context.

		# x = np.concatenate([window, wrong_window], axis=0) # [targetMasked, sourceFrame] :: MBx6xHxW
		# x = torch.FloatTensor(x)
		# mel = torch.FloatTensor(mel.T).unsqueeze(0) ## targetFrameMel :: MBx16x80
		# indiv_mels = torch.FloatTensor(indiv_mels).unsqueeze(1) ## targetMel :: MBxTx80x16
		# y = torch.FloatTensor(y) ## targetFrame :: MBx3xHxW
		# return x, indiv_mels, mel, y
	
	def forward(self, audio_sequences, face_sequences):
		return self.repoModel.forward(audio_sequences, face_sequences)

	@overrides
	def loadModel(self, path):
		# TODO: well, we have access to optimizer too in the pkl.
		assert False, "Not supported for model from internets."

	@overrides
	def loadWeights(self, path, yolo=False):
		doLoad(self.repoModel, path)