import torch as tr
import torch.nn as nn
from ...base_models.conv import Conv2d

class FaceEncoder(nn.Module):
	def __init__(self, dIn=6):
		super(FaceEncoder, self).__init__()

		self.model = nn.ModuleList([
			# 96,96
			nn.Sequential(
				Conv2d(dIn, 16, kernel_size=7, stride=1, padding=3)
			),
			# 48,48
			nn.Sequential(
				Conv2d(16, 32, kernel_size=3, stride=2, padding=1),
				Conv2d(32, 32, kernel_size=3, stride=1, padding=1, residual=True),
				Conv2d(32, 32, kernel_size=3, stride=1, padding=1, residual=True)
			),
			# 24,24
			nn.Sequential(
				Conv2d(32, 64, kernel_size=3, stride=2, padding=1),
				Conv2d(64, 64, kernel_size=3, stride=1, padding=1, residual=True),
				Conv2d(64, 64, kernel_size=3, stride=1, padding=1, residual=True),
				Conv2d(64, 64, kernel_size=3, stride=1, padding=1, residual=True)
			),
			#12,12
			nn.Sequential(
				Conv2d(64, 128, kernel_size=3, stride=2, padding=1),
				Conv2d(128, 128, kernel_size=3, stride=1, padding=1, residual=True),
				Conv2d(128, 128, kernel_size=3, stride=1, padding=1, residual=True)
			),
			# 6,6
			nn.Sequential(
				Conv2d(128, 256, kernel_size=3, stride=2, padding=1),
				Conv2d(256, 256, kernel_size=3, stride=1, padding=1, residual=True),
				Conv2d(256, 256, kernel_size=3, stride=1, padding=1, residual=True)
			),
			# 3,3
			nn.Sequential(
				Conv2d(256, 512, kernel_size=3, stride=2, padding=1),
				Conv2d(512, 512, kernel_size=3, stride=1, padding=1, residual=True)
			),
			# 1,1
			nn.Sequential(
				Conv2d(512, 512, kernel_size=3, stride=1, padding=0),
				Conv2d(512, 512, kernel_size=1, stride=1, padding=0)
			)
		])

	def forward(self, faceSequences):
		# faceSequences :: (B, 6, 96, 96)
		assert len(faceSequences.shape) == 4

		faceEmbeddingList = []
		x = faceSequences
		for f in self.model:
			x = f(x)
			faceEmbeddingList.append(x)
		return faceEmbeddingList