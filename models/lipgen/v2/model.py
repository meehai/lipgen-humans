# v1 
import torch as tr
from overrides import overrides
from .audio_encoder import AudioEncoder
from .face_encoder import FaceEncoder
from .face_decoder import FaceDecoder
from ...base_model import BaseModel
from neural_wrappers.pytorch import device

def reconstructionLoss(y, t):
	return (y - t).abs().mean()

class Model(BaseModel):
	def __init__(self, config):
		super().__init__(config)
		T = self.hyperParameters["context"] * 2 + 1
		self.audioEncoder = AudioEncoder(dIn=T)
		self.faceEncoder = FaceEncoder(dIn=6 * T)
		self.faceDecoder = FaceDecoder()

	@overrides
	def networkAlgorithm(self, trInputs, trLabels, isTraining, isOptimizing):
		X, t = trInputs, trLabels
		assert self.hyperParameters["context"] >= 1
		assert X["targetMelContext"].shape[1] == self.hyperParameters["context"] * 2
		assert X["targetContext"].shape[1] == self.hyperParameters["context"] * 2
		assert X["sourceContext"].shape[1] == self.hyperParameters["context"] * 2
		B, D, H, W = X["targetFrame"].shape
		mask = tr.ones(H, W).to(device)
		mask[H // 2:] = 0
		mel = X["targetMel"].unsqueeze(dim=1)

		# audioSequences :: (B, T, 1, 80, 16)
		audioSequences = tr.cat([mel, X["targetMelContext"]], dim=1).unsqueeze(dim=2)
		# faceSequences :: (B, T, 6, 96, 96)
		faceContext = tr.cat([trInputs["targetContext"] * mask, trInputs["sourceContext"]], dim=2)
		faceMiddle = tr.cat([trInputs["targetFrame"] * mask, trInputs["sourceFrame"]], dim=1).unsqueeze(dim=1)
		faceSequences = tr.cat([faceMiddle, faceContext], dim=1)

		# gt :: (B, 3, 96, 96)
		gt = trInputs["targetFrame"]

		faceEmbeddingList = self.faceEncoder(faceSequences)
		audioEmbedding = self.audioEncoder(audioSequences)
		faceEmbedding = faceEmbeddingList[-1]
		g = self.faceDecoder(faceEmbeddingList, audioEmbedding)

		gLoss = reconstructionLoss(g, gt)
		self.updateOptimizer(gLoss, isTraining, isOptimizing)
		targetGenerated = g.detach()

		syncLoss = self.expertSyncLoss(mel, targetGenerated)

		trResults = {
			"targetMasked" : trInputs["targetFrame"] * mask,
			"targetGenerated" : targetGenerated,
			"GLoss" : gLoss,
			"expertSyncLoss" : syncLoss
		}

		return trResults, gLoss

	@overrides
	def forward(self, audio_sequences, face_sequences):
		#TODO: Send context from inference_video instead of replicating same frame
		context = self.hyperParameters["context"] * 2 + 1
		audio_sequences = audio_sequences.unsqueeze(dim=1).repeat(1, context, 1, 1, 1)
		face_sequences = face_sequences.unsqueeze(dim=1).repeat(1, context, 1, 1, 1)
		return super().forward(audio_sequences, face_sequences)