# Simple unsupervised prediction.
import numpy as np
import torch as tr
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from overrides import overrides
from neural_wrappers.pytorch import GANOptimizer, FeedForwardNetwork, device
from neural_wrappers.callbacks import SaveModels, PlotMetrics

from ..base_models import LipGen, AudioEncoder, FaceEncoder, FaceDecoder

def bce(y, t):
	return -(t * tr.log(y + np.spacing(1)) + (1 - t) * tr.log(1 - y + np.spacing(1))).mean()

def syncLoss(face, audio, t):
	face = F.normalize(face, p=2, dim=1)
	audio = F.normalize(audio, p=2, dim=1)
	# 0 diff = match, however we need 1 - L2 so it's similarity.
	L2 = ((face - audio)**2).mean(dim=1)
	y = 1 - L2
	return bce(y, t)

def reconstructionLoss(y, t):
	return (y - t).abs().mean()

class Generator(FeedForwardNetwork):
	def __init__(self):
		super().__init__()
		self.audioEncoder = AudioEncoder()
		self.faceEncoder = FaceEncoder(dIn=6)
		self.faceDecoder = FaceDecoder()

	def forward(self, audio_sequences, face_sequences):
		audioEmbedding = self.audioEncoder(audio_sequences)
		faceEmbeddingList = self.faceEncoder(face_sequences)
		faceEmbedding = faceEmbeddingList[-1]
		g = self.faceDecoder(faceEmbeddingList, audioEmbedding)
		return g

class Discriminator(FeedForwardNetwork):
	def __init__(self):
		super().__init__()

		self.audioEncoder = AudioEncoder()
		self.faceEncoder = FaceEncoder(dIn=3)

	def forward(self, trInputs):
		# audio_sequences = (B, 80, 16) => (B, 1, 80, 16)
		audioEmbedding = self.audioEncoder(trInputs["targetMel"].unsqueeze(dim=1))
		faceEmbeddingList = self.faceEncoder(trInputs["targetFrame"])
		faceEmbedding = faceEmbeddingList[-1]
		return faceEmbedding.squeeze(), audioEmbedding.squeeze()

class Model(LipGen):
	def __init__(self, config):
		super().__init__(config)
		assert "gSyncLoss" in config["lossWeights"]

		self.generator = Generator()
		self.discriminator = Discriminator()
		metrics = {
			"L1 RGB" : lambda y, t, **k : np.abs(y["targetGenerated"] - t["targetFrame"]).mean() * 255,
			"GLoss" : lambda y, t, **k : y["GLoss"],
			"GSyncLoss" : lambda y, t, **k : y["GSyncLoss"],
			"GReconstructionLoss" : lambda y, t, **k : y["GReconstructionLoss"],
			"DLoss" : lambda y, t, **k : y["DLoss"],
			"DSyncLoss" : lambda y, t, **k : y["DSyncLoss"]
		}
		self.addMetrics(metrics)
		self.addCallbacks([PlotMetrics(["Loss", *list(metrics.keys())]), SaveModels("best", "L1 RGB"), \
			SaveModels("best", "GLoss")])

	@overrides
	def networkAlgorithm(self, trInputs, trLabels, isTraining, isOptimizing):
		MB = len(trInputs["sourceFrame"])
		ones = tr.full((MB, ), 1, dtype=tr.float32).to(device)
		zeros = tr.full((MB, ), 0, dtype=tr.float32).to(device)

		# Generate fake data
		audio_sequences = trInputs["targetMel"].unsqueeze(dim=1)
		face_sequences = tr.cat([trInputs["targetMasked"], trInputs["sourceFrame"]], dim=1)
		g = self.generator.forward(audio_sequences, face_sequences)

		# Discriminator step
		dFakeInput = {"targetMel" : trInputs["targetMel"], "targetFrame" : g.detach()}
		dRealInput = {"targetMel" : trInputs["targetMel"], "targetFrame" : trInputs["targetFrame"]}
		dFakeFace, dFakeAudio = self.discriminator.forward(dFakeInput)
		dRealFace, dRealAudio = self.discriminator.forward(dRealInput)
		dSyncRealLoss = syncLoss(dRealFace, dRealAudio, ones)
		dSyncFakeLoss = syncLoss(dFakeFace, dFakeAudio, zeros)
		dSyncLoss = (dSyncRealLoss + dSyncFakeLoss) / 2
		dLoss = dSyncLoss
		self.discriminator.updateOptimizer(dLoss, isTraining, isOptimizing)

		# Generator step
		gFakeInput = {"targetMel" : trInputs["targetMel"], "targetFrame" : g}
		gFakeFace, gFakeAudio = self.discriminator.forward(gFakeInput)
		gSyncLoss = self.hyperParameters["lossWeights"]["gSyncLoss"] * syncLoss(gFakeFace, gFakeAudio, ones)
		gReconstructionLoss = self.hyperParameters["lossWeights"]["gReconstructionLoss"] \
			* reconstructionLoss(g, trInputs["targetFrame"])
		gLoss = (gSyncLoss + gReconstructionLoss) / 2
		self.generator.updateOptimizer(gLoss, isTraining, isOptimizing)

		trResults = {
			"targetGenerated" : g.detach(),
			"GLoss" : gLoss.detach(),
			"GSyncLoss" : gSyncLoss.detach(),
			"GReconstructionLoss" : gReconstructionLoss.detach(),
			"DLoss" : dLoss.detach(),
			"DSyncLoss" : dSyncLoss.detach()
		}
		trLoss = (dLoss + gLoss) / 2
		return trResults, trLoss
	
	# Genertor only, used for video inference and others
	@overrides
	def forward(self, audio_sequences, face_sequences):
		return self.generator.forward(audio_sequences, face_sequences)

	@overrides
	def setOptimizer(self, optimizer, **kwargs):
		assert not isinstance(optimizer, optim.Optimizer)
		ganOptimizer = GANOptimizer(self, optimizer, **kwargs)
		super().setOptimizer(ganOptimizer, **kwargs)