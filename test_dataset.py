import matplotlib.pyplot as plt
import numpy as np
from neural_wrappers.callbacks import Callback

class PlotCallback(Callback):
	def onIterationEnd(self, results, labels, **kwargs):
		data = kwargs["data"]
		MB = len(results["targetGenerated"])

		for j in range(MB):
			ax = plt.subplots(1, 5)[1]
			[axi.set_axis_off() for axi in ax.ravel()]
			diff = np.abs(labels["targetFrame"][j] - results["targetGenerated"][j])
			ax[0].imshow(data["sourceFrame"][j].transpose(1, 2, 0))
			ax[1].imshow(labels["targetFrame"][j].transpose(1, 2, 0))
			ax[2].imshow(results["targetGenerated"][j].transpose(1, 2, 0))
			ax[3].imshow(diff.transpose(1, 2, 0), cmap="hot")
			ax[4].imshow(data["targetMel"][j])
			plt.show()
			plt.close()

def test_dataset(model, generator):
	model.addCallbacks([PlotCallback()])
	res = model.testGenerator(generator)
	print(res)
