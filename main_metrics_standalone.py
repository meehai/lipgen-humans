import yaml
import torch as tr
import numpy as np
from models import getModel
from neural_wrappers.pytorch import device, trModuleWrapper
from neural_wrappers.utilities import RunningMean
from main import getReader
from argparse import ArgumentParser
from tqdm import trange

def cosine_similarity(a, b):
	Dot = (a * b).sum(axis=1)
	Norm = np.linalg.norm(a, ord=2, axis=1) * np.linalg.norm(b, ord=2, axis=1)
	y = Dot / (Norm + np.spacing(1))
	return y

def bce(y, t):
	y = y + np.spacing(1)
	return -(t * np.log(y) + (1 - t) * np.log(1 - y))

def fSyncLoss(a, v, t):
	return bce(cosine_similarity(a, v), t)

def fL1Px(y, t):
	MB = y.shape[0]
	assert y.shape == t.shape
	y = y.reshape(MB, -1)
	t = t.reshape(MB, -1)
	L1 = np.abs(y - t).mean(axis=1) * 255
	return L1

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("modelCfg")
	parser.add_argument("weightsFile")
	parser.add_argument("dataCfg")
	args = parser.parse_args()
	args.dataCfg = yaml.load(open(args.dataCfg, "r"), Loader=yaml.SafeLoader)
	args.modelCfg = yaml.load(open(args.modelCfg, "r"), Loader=yaml.SafeLoader)
	return args

def main():
	args = getArgs()
	context = args.modelCfg["context"]
	model = getModel(args.modelCfg).to(device)
	modelLipSync = trModuleWrapper(model.baseSyncNet)
	model.loadWeights(args.weightsFile)
	model.eval()
	modelLipSync.eval()

	reader = getReader(args.dataCfg["datasetPath"], batchSize=args.dataCfg["batchSize"], context=context)
	generator = reader.iterate(maxPrefetch=1)
	H, W = reader.resolution
	mask = np.ones((H, W))
	mask[H // 2:] = 0

	rmSync, rmL1 = RunningMean(0), RunningMean(0)
	Range = trange(len(generator))
	ones = np.ones((args.dataCfg["batchSize"]), dtype=np.float32)
	for i in Range:
		item, B = next(generator)
		data = item[0]

		# mel :: MB,1,80,16
		mel = data["targetMel"][:, None]
		t = data["targetFrame"]
		s = data["sourceFrame"]
		MB = len(mel)
		# f :: MB,6,96,96
		f = np.concatenate([t * mask, s], axis=1).astype(np.float32)
		# g :: MB,3,96,96
		g = model.npForward(mel, f)

		# gSync :: MB,3,1,96,96
		gSync = g[:, :, None]
		# gSync :: MB,3,T,96,96
		gSync = np.tile(gSync, (1, 1, modelLipSync._module.T, 1, 1))
		# gSync :: MB,3,T,48,96
		gSync = gSync[:, :, :, g.shape[3] // 2:]
		# gSync :: MB,3*T,48,96
		gSync = np.concatenate([gSync[:, :, i] for i in range(modelLipSync._module.T)], axis=1)
		# a :: MB x 512; v :: MB x 512
		a, v = modelLipSync.npForward(mel, gSync)

		syncLoss = fSyncLoss(a, v, ones[0 : MB])
		L1Loss = fL1Px(g, t)
		rmL1.updateBatch(L1Loss)
		rmSync.updateBatch(syncLoss)
		if i % 50 == 0:
			Range.set_postfix({"Sync": "%2.2f" % rmSync.get(), "L1 (px)" : "%2.2f" % rmL1.get()})
	print("Lip sync: %2.5f" % rmSync.get())
	print("L1 (px) loss: %2.5f" % rmL1.get())

if __name__ == "__main__":
	main()